﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wypozyczalnia.Extensions;
using System.Collections.Generic;
using FluentAssertions;

namespace Wypozyczalnia.Models.Tests
{
    [TestClass()]
    public class CarTests
    {
        [TestMethod()]
        public void ValidateVINNumber_TestPositive()
        {
            Car car = new Car
            {
                Vin = "1G4PP5SK0E4200085"
            };

            Dictionary<bool, string> dictionary = car.ValidateVINNumber();
            dictionary.Should().ContainKey(true);
            dictionary.Should().ContainValue("");
        }

        [TestMethod()]
        public void ValidateVINNumber_TestNegative_TooShort()
        {
            Car car = new Car
            {
                Vin = "1G4PP5SK0E420008"
            };

            Dictionary<bool, string> dictionary = car.ValidateVINNumber();
            dictionary.Should().ContainKey(false);
            dictionary.Should().ContainValue("Wprowadzony numer VIN samochodu jest nieprawiłowy! (16 znaków zamiast 17)\n");
        }

        [TestMethod()]
        public void ValidateVINNumber_TestNegative_Empty()
        {
            Car car = new Car
            {
                Vin = ""
            };

            Dictionary<bool, string> dictionary = car.ValidateVINNumber();
            dictionary.Should().ContainKey(false);
            dictionary.Should().ContainValue("Numer VIN samochodu nie może zostać pusty!\n");
        }

        [TestMethod()]
        public void ValidateVINNumber_TestNegative_IncorrectSign()
        {
            Car car = new Car
            {
                Vin = "1G4PP5SK0E42000O5" +
                ""
            };

            Dictionary<bool, string> dictionary = car.ValidateVINNumber();
            dictionary.Should().ContainKey(false);
            dictionary.Should().ContainValue("Wprowadzony numer VIN samochodu jest nieprawiłowy! (Numer VIN nie może zawierać liter I, O i Q!\n");
        }

        [TestMethod()]
        public void ValidateLicensePlatenumber_TestPositive()
        {
            Car car = new Car
            {
                License_plate_number = "RZE1234"
            };

            Dictionary<bool, string> dictionary = car.ValidateLicensePlatenumber();
            dictionary.Should().ContainKey(true);
            dictionary.Should().ContainValue("");
        }

        [TestMethod()]
        public void ValidateLicensePlatenumber_TestNegative_TooShort()
        {
            Car car = new Car
            {
                License_plate_number = "RZE123"
            };

            Dictionary<bool, string> dictionary = car.ValidateLicensePlatenumber();
            dictionary.Should().ContainKey(false);
            dictionary.Should().ContainValue("Wprowadzony numer rejestracyjny samochodu jest nieprawiłowy! (6 znaków zamiast 7)");
        }

        [TestMethod()]
        public void ValidateLicensePlatenumber_TestNegative_Empty()
        {
            Car car = new Car
            {
                License_plate_number = ""
            };

            Dictionary<bool, string> dictionary = car.ValidateLicensePlatenumber();
            dictionary.Should().ContainKey(false);
            dictionary.Should().ContainValue("Numer rejestracyjny samochodu nie może zostać pusty!");
        }

        [TestMethod()]
        public void ValidateLicensePlatenumber_TestNegative_IncorrectSignsOrder()
        {
            Car car = new Car
            {
                License_plate_number = "1234567"
            };

            Dictionary<bool, string> dictionary = car.ValidateLicensePlatenumber();
            dictionary.Should().ContainKey(false);
            dictionary.Should().ContainValue("Wprowadzony numer rejestracyjny samochodu jest nieprawiłowy! (Co najmniej 2 pierwsze znaki muszą być literami)");
        }
    }
}