﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wypozyczalnia.Models;
using Wypozyczalnia.Services;
using Wypozyczalnia.Views;

namespace Wypozyczalnia.Presenters
{
    public class RentCarPresenter
    {
        private readonly IRentCar rentCarView;
        private readonly RentService rentService;
        private readonly CarService carService;
        private readonly ClientService clientService;
        Car car;

        public RentCarPresenter(IRentCar view)
        {
            rentCarView = view;
            rentService = new RentService();
            carService = new CarService();
            clientService = new ClientService();
        }

        public void FillForm(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            if (sender is DataGridView)
            {
                var cells = (sender as DataGridView).CurrentRow.Cells;
                car = carService.GetCarByLicensePlateNum(cells[11].FormattedValue.ToString());
                if (car != null)
                {
                    rentCarView.Manufacturer = car.Manufacturer;
                    rentCarView.Model = car.Model;
                    rentCarView.Engine = car.Engine_type;
                    rentCarView.LicensePlateNumber = car.License_plate_number;
                    rentCarView.Cost = car.Rent_cost.ToString();
                }
            }
        }

        public void FillRentData(Rent rent)
        {
            rentCarView.ClientName = rent.Client.Name;
            rentCarView.ClientSurname = rent.Client.Surname;
            rentCarView.LicenseNumber = rent.Client.License_number;

            rentCarView.DateStart = rent.Start_date;
            rentCarView.DateEnd = rent.End_date;

            rentCarView.TotalCost = rent.Total_cost;

            rentCarView.Manufacturer = rent.Car.Manufacturer;
            rentCarView.Model = rent.Car.Model;
            rentCarView.Engine = car.Engine_type;
            rentCarView.LicensePlateNumber = rent.Car.License_plate_number;
            rentCarView.Cost = rent.Car.Rent_cost.ToString();
        }

        public void CalculateTotalCost()
        {
            int startDay = rentCarView.DateStart.DayOfYear;
            int endDay = rentCarView.DateEnd.DayOfYear;
            decimal cost;
            if (decimal.TryParse(rentCarView.Cost, out cost))
            {
                if (endDay >= startDay)
                {
                    rentCarView.TotalCost = (endDay - startDay) * cost;
                }
            }
        }

        public Dictionary<bool, string> ValidateDates()
        {
            Dictionary<bool, string> validationResult = new Dictionary<bool, string>();
            bool valid = true;
            string errorMsg = string.Empty;

            if (rentCarView.DateStart.Date < DateTime.Now.Date)
            {
                valid = false;
                errorMsg += "Data wypożyczenia nie może być wcześniejsza niż dzień obecny!\n";
            }
            else if (rentCarView.DateEnd.Date < DateTime.Now.Date)
            {
                valid = false;
                errorMsg += "Data zwrotu nie może być wcześniejsza niż dzień obecny!\n";
            }
            else if (rentCarView.DateStart.Date > rentCarView.DateEnd.Date)
            {
                valid = false;
                errorMsg += "Data zwrotu nie może być wcześniejsza niż data wypożyczenia!\n";
            }
            else if (rentCarView.DateStart.Date == rentCarView.DateEnd.Date)
            {
                valid = false;
                errorMsg += "Data wypożyczenia i zwrotu nie mogą być identyczne!\n";
            }

            validationResult.Add(valid, errorMsg);
            return validationResult;
        }

        public Dictionary<bool, string> ValidateClientData()
        {
            Dictionary<bool, string> validationResult = new Dictionary<bool, string>();
            bool valid = true;
            string errorMsg = string.Empty;

            if (string.IsNullOrWhiteSpace(rentCarView.ClientName))
            {
                valid = false;
                errorMsg += "Imię klienta nie może zostać puste!\n";
            }
            if (string.IsNullOrWhiteSpace(rentCarView.ClientSurname))
            {
                valid = false;
                errorMsg += "Nazwisko klienta nie może zostać puste!\n";
            }
            if (string.IsNullOrWhiteSpace(rentCarView.LicenseNumber))
            {
                valid = false;
                errorMsg += "Numer prawa jazdy nie może zostać pusty!\n";
            }

            validationResult.Add(valid, errorMsg);
            return validationResult;
        }

        public void AddNewRent()
        {
            var car = carService.GetCarByLicensePlateNum(rentCarView.LicensePlateNumber);
            var client = clientService.GetClientByLicenceNumber(rentCarView.LicenseNumber);
            if (client == null)
            {
                client = clientService.AddNewClient(rentCarView.ClientName, rentCarView.ClientSurname, rentCarView.LicenseNumber);
            }

            var rent = new Rent { Client = client, Car = car, Start_date = rentCarView.DateStart, End_date = rentCarView.DateEnd, Total_cost = rentCarView.TotalCost };
            try
            {
                rentService.CreateNewRent(rent);
                carService.SetStatusAsRent(car);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void ReturnCar()
        {
            carService.SetStatusAsAvailable(carService.GetCarByLicensePlateNum(rentCarView.LicensePlateNumber));
        }
    }
}
