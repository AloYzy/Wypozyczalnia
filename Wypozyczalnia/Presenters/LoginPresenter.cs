﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wypozyczalnia.Services;
using Wypozyczalnia.Models;
using Wypozyczalnia.Views;

namespace Wypozyczalnia.Presenters
{
    public class LoginPresenter
    {
        private readonly ILogin loginView;
        private readonly LoginService loginService;

        public LoginPresenter(ILogin view)
        {
            loginView = view;
            loginService = new LoginService();
        }

        public void LogIn()
        {
            var user = new User()
            {
                Login = loginView.LoginText,
                Password = loginView.PasswordText
            };

            loginView.LogedIn = loginService.IsUserLoggedIn(user);
        }
    }
}
