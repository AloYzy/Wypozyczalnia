﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wypozyczalnia.Extensions;
using Wypozyczalnia.Models;
using Wypozyczalnia.Services;
using Wypozyczalnia.Views;

namespace Wypozyczalnia.Presenters
{
    public class ModifyCarPresenter
    {
        private readonly IModifyCar modifyCarView;
        private readonly CarService carService;
        private Car _car;

        public ModifyCarPresenter(IModifyCar view)
        {
            modifyCarView = view;
            carService = new CarService();
        }

        public void ValidateSearchCriteria()
        {
            Dictionary<bool, string> validationResult = new Dictionary<bool, string>();
            bool valid = true;
            string errorMsg = string.Empty;
            var car = new Car(); //create temporary Car object - only to call extensions methods to validate license plate number and VIN

            if (!string.IsNullOrWhiteSpace(modifyCarView.SearchLicensePlaneNumText))
            {
                car.License_plate_number = modifyCarView.SearchLicensePlaneNumText;
                if (car.ValidateLicensePlatenumber().Keys.Contains(false))
                {
                    valid = false;
                    errorMsg += car.ValidateLicensePlatenumber()[false];
                }
                else if (!carService.IsLicensePlateNumberPresentInDB(modifyCarView.SearchLicensePlaneNumText))
                {
                    valid = false;
                    errorMsg += "Podany numer rejestracyjny nie istnieje w bazie!";
                }
            }
            else if (!string.IsNullOrWhiteSpace(modifyCarView.SearchVINText))
            {
                car.Vin = modifyCarView.SearchVINText;
                if (car.ValidateVINNumber().Keys.Contains(false))
                {
                    valid = false;
                    errorMsg += car.ValidateVINNumber()[false];
                }
                else if (!carService.IsVINPresentInDB(modifyCarView.SearchVINText))
                {
                    valid = false;
                    errorMsg += "Podany numer VIN nie istnieje w bazie!";
                }
            }
            else
            {
                valid = false;
                errorMsg += "Podaj numer VIN lub numer rejestracyjny samochodu!";
            }

            validationResult.Add(valid, errorMsg);
            modifyCarView.ValidationResult = validationResult;
        }

        public void LoadCarData()
        {
            var car = !string.IsNullOrEmpty(modifyCarView.SearchLicensePlaneNumText) ? carService.GetCarByLicensePlateNum(modifyCarView.SearchLicensePlaneNumText) : carService.GetCarByVIN(modifyCarView.SearchVINText);

            if (car != null)
            {
                _car = car;

                modifyCarView.CategoryText = car.Type;
                modifyCarView.CostText = car.Rent_cost.ToString();
                modifyCarView.DriveTypeText = car.Drive_type;
                modifyCarView.EngineText = car.Engine_type;
                modifyCarView.GearboxText = car.Gearbox_type;
                modifyCarView.LicensePlateNumText = car.License_plate_number;
                modifyCarView.ManufacturerText = car.Manufacturer;
                modifyCarView.ModelText = car.Model;
                modifyCarView.ProductionDate = car.Production_year;
                modifyCarView.VINText = car.Vin;
            }
        }

        public void SaveModification()
        {
            _car.Type = modifyCarView.CategoryText;
            _car.Rent_cost = decimal.Parse(modifyCarView.CostText);
            _car.Drive_type = modifyCarView.DriveTypeText;
            _car.Engine_type = modifyCarView.EngineText;
            _car.Gearbox_type = modifyCarView.GearboxText;
            _car.License_plate_number = modifyCarView.LicensePlateNumText;
            _car.Manufacturer = modifyCarView.ManufacturerText;
            _car.Model = modifyCarView.ModelText;
            _car.Production_year = modifyCarView.ProductionDate;
            _car.Vin = modifyCarView.VINText;

            try
            {
                carService.UpdateCar(_car);
            }
            catch (Exception)
            {

                throw;
            }
            

            //Modification modification = new Modification
            //{
            //    Car = new Car
            //    {
            //        Category = modifyCarView.CategoryText,
            //        Cost = modifyCarView.CostText,
            //        DriveType = modifyCarView.DriveTypeText,
            //        Engine = modifyCarView.EngineText,
            //        Gearbox = modifyCarView.GearboxText,
            //        LicensePlateNum = modifyCarView.LicensePlateNumText,
            //        Manufacturer = modifyCarView.ManufacturerText,
            //        Model = modifyCarView.ModelText,
            //        ProductionDate = modifyCarView.ProductionDate,
            //        VIN = modifyCarView.VINText
            //    },
            //    Reason = modifyCarView.ReasonText,
            //    SearchLicensePlaneNumText = modifyCarView.SearchLicensePlaneNumText,
            //    SearchVINText = modifyCarView.SearchVINText
            //};

            //return modification.SaveModification();
        }
    }
}

