﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wypozyczalnia.Enums;
using Wypozyczalnia.Models;
using Wypozyczalnia.Services;
using Wypozyczalnia.Views;

namespace Wypozyczalnia.Presenters
{
    public class DeleteCarPresenter
    {
        private readonly IDeleteCar deleteCarView;
        private readonly CarService carService;

        public DeleteCarPresenter(IDeleteCar view)
        {
            deleteCarView = view;
            carService = new CarService();
        }

        public void DeleteCar()
        {
            Car car = null;

            if (deleteCarView.LicensePlateNumber.Length > 0)
            {
                car = carService.GetCarByLicensePlateNum(deleteCarView.LicensePlateNumber);
            }
            else if (deleteCarView.VIN.Length > 0)
            {
                car = carService.GetCarByVIN(deleteCarView.VIN);
            }

            if(car != null)
            {
                carService.SetStatusAsDeleted(car);
            }
        }

        public bool CanDeleteCar()
        {
            bool canDelete = false;
            Car car = null;

            if (deleteCarView.LicensePlateNumber.Length > 0)
            {
                car =  carService.GetCarByLicensePlateNum(deleteCarView.LicensePlateNumber);
            }
            else if (deleteCarView.VIN.Length > 0)
            {
                car = carService.GetCarByVIN(deleteCarView.VIN);
            }

            if (car != null && car.Status.Equals(CarEnums.EnumStatus.Dostępny.ToString()))
            {
                canDelete = true;
            }

            return canDelete;
        }

    }
}
