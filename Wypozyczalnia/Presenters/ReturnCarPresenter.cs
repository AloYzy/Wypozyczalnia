﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wypozyczalnia.Enums;
using Wypozyczalnia.Models;
using Wypozyczalnia.Services;
using Wypozyczalnia.Views;
using static Wypozyczalnia.Enums.CarEnums;

namespace Wypozyczalnia.Presenters
{
    public class ReturnCarPresenter
    {
        private readonly IReturnCar returnCarView;
        private readonly RentService rentService;
        private readonly CarService carService;

        public ReturnCarPresenter(IReturnCar view)
        {
            returnCarView = view;
            rentService = new RentService();
            carService = new CarService();
        }

        public void CheckIfSearchPossible()
        {
            if (returnCarView.LicenseNumber.Length == 7)
            {
                returnCarView.SearchPossible = true;
            }
        }

        public bool FindRent()
        {
            bool found = false;

            var car = carService.GetCarByLicensePlateNum(returnCarView.LicenseNumber);
            if (car != null && !string.IsNullOrEmpty(car.License_plate_number) && car.Status.Trim().Equals(CarEnums.EnumStatus.Wypożyczony.ToString()))
            {
                Rent rent = rentService.GetRentByCarId(car.Id);
                if (rent != null)
                {
                    found = true;
                    returnCarView.Rent = rent;
                }
            }

            return found;
        }
    }
}
