﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wypozyczalnia.Services;
using Wypozyczalnia.Models;
using Wypozyczalnia.Views;

namespace Wypozyczalnia.Presenters
{
    public class SearchCarPresenter
    {
        private readonly IShowCars showCarsView;
        private readonly CarService carService;
        private readonly Dictionary<ICollection<string>, CheckedListBox> checkedListBoxTypesDictionary;

        #region CTOR
        public SearchCarPresenter(IShowCars view)
        {
            showCarsView = view;
            carService = new CarService();

            checkedListBoxTypesDictionary = new Dictionary<ICollection<string>, CheckedListBox>
            {
                { carService.GetAllCategories(), showCarsView.CategoryCheckedListBox },
                { carService.GetAllDriveTypes(), showCarsView.DriveTypeCheckedListBox },
                { carService.GetAllEngineTypes(), showCarsView.EngineCheckedListBox },
                { carService.GetAllManufacturers(), showCarsView.ManufacturerCheckedListBox },
                { carService.GetAllModels(), showCarsView.ModelCheckedListBox }
            };
        }
        #endregion

        public void SearchAllAvailableCars()
        {
            showCarsView.AllAvailableCars = carService.GetAllAvailableCars();
        }

        public void ShowAllCars()
        {
            showCarsView.AllCars = carService.GetAllCars();
        }

        public void FillCheckedListBoxes()
        {
            foreach (var checkedListBox in checkedListBoxTypesDictionary)
            {
                var checkedListeBoxItemsCollection = checkedListBox.Key;
                var checkedListeBoxControl = checkedListBox.Value;

                foreach (var item in checkedListeBoxItemsCollection)
                {
                    checkedListeBoxControl.Items.Add(item);
                }
            }
        }

        public void SearchAvailableCarsByCriteria(CheckedListBox.CheckedItemCollection categoryItems, CheckedListBox.CheckedItemCollection manufacturerItems, CheckedListBox.CheckedItemCollection modelItems, CheckedListBox.CheckedItemCollection driveTypeItems, CheckedListBox.CheckedItemCollection engineTypeItems, DateTime productionDateFrom, DateTime productionDateTo, string costTextFrom, string costTextTo, string gearboxSelectionText)
        {
            var searchCriteria = PrepareSearchCriteria(categoryItems,
                                                       manufacturerItems,
                                                       modelItems,
                                                       driveTypeItems,
                                                       engineTypeItems,
                                                       productionDateFrom,
                                                       productionDateTo,
                                                       costTextFrom,
                                                       costTextTo,
                                                       gearboxSelectionText);

            //showCarsView.SearchedAvailableCars = carService.SearchAvailableCarsByCriteria(searchCriteria);
        }

        private SearchCriteria PrepareSearchCriteria(CheckedListBox.CheckedItemCollection categoryItems, CheckedListBox.CheckedItemCollection manufacturerItems, CheckedListBox.CheckedItemCollection modelItems, CheckedListBox.CheckedItemCollection driveTypeItems, CheckedListBox.CheckedItemCollection engineTypeItems, DateTime productionDateFrom, DateTime productionDateTo, string costTextFrom, string costTextTo, string gearboxSelectionText)
        {
            string categoryCriteria = ConvertCheckedItemCollectionToString(categoryItems);
            string manufacturerCriteria = ConvertCheckedItemCollectionToString(manufacturerItems);
            string modelCriteria = ConvertCheckedItemCollectionToString(modelItems);
            string driveTypeCriteria = ConvertCheckedItemCollectionToString(driveTypeItems);
            string engineTypeCriteria = ConvertCheckedItemCollectionToString(engineTypeItems);

            decimal.TryParse(costTextFrom, out decimal costFrom);
            decimal.TryParse(costTextTo, out decimal costTo);
            decimal costToCriteria = carService.GetMaxCost();

            return new SearchCriteria()
            {
                CategoryCriteria = categoryCriteria,
                ManufacturerCriteria = manufacturerCriteria,
                ModelCriteria = modelCriteria,
                DriveTypeCriteria = driveTypeCriteria,
                EngineTypeCriteria = engineTypeCriteria,
                ProductionDateFromCriteria = productionDateFrom,
                ProductionDateToCriteria = productionDateTo,
                CostFromCriteria = costFrom,
                CostToCriteria = costTo == 0m ? costToCriteria : costTo, // if costTo == 0 then probably parsing didn't worked so default max cost will be used
                GearboxCriteria = gearboxSelectionText
            };
        }

        private string ConvertCheckedItemCollectionToString(CheckedListBox.CheckedItemCollection checkedItemCollection)
        {
            StringBuilder criteria = new StringBuilder(string.Empty);

            if (checkedItemCollection.Count > 0)
            {
                var enumerator = checkedItemCollection.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    criteria.Append("'" + enumerator.Current.ToString().Trim() + "'" + ',');
                }

                criteria.Remove(criteria.Length - 1, 1);
            }

            return criteria.ToString();
        }

        public struct SearchCriteria
        {
            public string CategoryCriteria { get; set; }
            public string ManufacturerCriteria { get; set; }
            public string ModelCriteria { get; set; }
            public string DriveTypeCriteria { get; set; }
            public string EngineTypeCriteria { get; set; }
            public DateTime ProductionDateFromCriteria { get; set; }
            public DateTime ProductionDateToCriteria { get; set; }
            public decimal CostFromCriteria { get; set; }
            public decimal CostToCriteria { get; set; }
            public string GearboxCriteria { get; set; }
        }

    }
}
