﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wypozyczalnia.Models;
using Wypozyczalnia.Services;
using Wypozyczalnia.Views;

namespace Wypozyczalnia.Presenters
{
    public class AddCarPresenter
    {
        private readonly IAddCar addCarView;
        private readonly CarService carService;

        public AddCarPresenter(IAddCar view)
        {
            addCarView = view;
            carService = new CarService();
        }

        public void AddNewCar()
        {
            Car car = new Car
            {
                Type = addCarView.CategoryText,
                Rent_cost = decimal.Parse(addCarView.CostText),
                Drive_type = addCarView.DriveTypeText,
                Engine_type = addCarView.EngineText,
                Gearbox_type = addCarView.GearboxText,
                License_plate_number = addCarView.LicensePlateNumText,
                Manufacturer = addCarView.ManufacturerText,
                Model = addCarView.ModelText,
                Production_year = addCarView.ProductionDate,
                Vin = addCarView.VINText
            };

            carService.AddNewCar(car);
        }

        public void ValidateCar()
        {
            if (decimal.TryParse(addCarView.CostText, out decimal cost))
            {
                Car car = new Car
                {
                    Type = addCarView.CategoryText,
                    Rent_cost = cost,
                    Drive_type = addCarView.DriveTypeText,
                    Engine_type = addCarView.EngineText,
                    Gearbox_type = addCarView.GearboxText,
                    License_plate_number = addCarView.LicensePlateNumText,
                    Manufacturer = addCarView.ManufacturerText,
                    Model = addCarView.ModelText,
                    Production_year = addCarView.ProductionDate,
                    Vin = addCarView.VINText
                };

                addCarView.Valid = carService.ValidateCar(car);
                return;
            }

            addCarView.Valid = new Dictionary<bool, string> { { false, "Podany koszt wynajęcia samochodu jest nieprawidłowy!\n" } };
        }

        public Dictionary<string, string[]> FillComboboxes()
        {
            var comboBoxesItems = new Dictionary<string, string[]>
            {
                { "category", carService.GetAllCategories().ToArray() },
                { "driveType", carService.GetAllDriveTypes().ToArray() },
                { "engine", carService.GetAllEngineTypes().ToArray() },
                { "gearbox", carService.GetAllGearboxTypes().ToArray() }
            };

            return comboBoxesItems;
        }

        public void ClearCarData()
        {
            Car car = new Car();

            addCarView.CategoryText = car.Type;
            addCarView.CostText = car.Rent_cost.ToString();
            addCarView.DriveTypeText = car.Drive_type;
            addCarView.EngineText = car.Engine_type;
            addCarView.GearboxText = car.Gearbox_type;
            addCarView.LicensePlateNumText = car.License_plate_number;
            addCarView.ManufacturerText = car.Manufacturer;
            addCarView.ModelText = car.Model;
            addCarView.ProductionDate = car.Production_year;
            addCarView.VINText = car.Vin;
        }
        
    }
}
