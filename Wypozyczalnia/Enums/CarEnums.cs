﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wypozyczalnia.Enums
{
    public static class CarEnums
    {
        /// <summary>
        /// Enum that contains all possible categories of Car.
        /// </summary>
        public enum EnumCategory
        {
            Hatchback,
            Kombi,
            Sedan,
            Suv
        }

        /// <summary>
        /// Enum that contains all possible drive types. 
        /// </summary>
        public enum EnumDriveType
        {
            AllDrive,
            Przedni,
            Tylny
        }

        /// <summary>
        /// Enum that contains all possible engine types.
        /// </summary>
        public enum EnumEngineType
        {
            Benzynowy,
            Diesel,
            Hybrydowy
        }

        /// <summary>
        /// Enum that contains all possible gearbox types.
        /// </summary>
        public enum EnumGearboxType
        {
            Automatyczna,
            Manualna
        }

        /// <summary>
        /// Enum that contains all possible statuses of car.
        /// </summary>
        public enum EnumStatus
        {
            Dostępny,
            Usunięty,
            Wypożyczony
        }
    }
}
