﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wypozyczalnia.Models;

namespace Wypozyczalnia.Extensions
{
    public static class CarExtensions
    {
        /// <summary>
        /// Validates if car data are correct.
        /// </summary>
        /// <returns>Dictionary with key = ture if data are correct, key = false if data are incorrect.</returns>
        public static Dictionary<bool, string> ValidateCar(this Car car)
        {
            bool valid = true;
            string errorMsg = string.Empty;
            Dictionary<bool, string> validationResult = new Dictionary<bool, string>();

            if (String.IsNullOrWhiteSpace(car.Type))
            {
                valid = false;
                errorMsg += "Klasa samochodu nie może zostać pusta!\n";
            }

            if (String.IsNullOrWhiteSpace(car.Manufacturer))
            {
                valid = false;
                errorMsg += "Marka samochodu nie może zostać pusta!\n";
            }

            if (String.IsNullOrWhiteSpace(car.Model))
            {
                valid = false;
                errorMsg += "Model samochodu nie może zostać pusty!\n";
            }

            if (String.IsNullOrWhiteSpace(car.Drive_type))
            {
                valid = false;
                errorMsg += "Rodzaj napędu nie może zostać pusty!\n";
            }

            if (String.IsNullOrWhiteSpace(car.Gearbox_type))
            {
                valid = false;
                errorMsg += "Skrzynia biegów nie może zostać pusta!\n";
            }

            if (car.Production_year == null)
            {
                valid = false;
                errorMsg += "Data produkcji nie może zostać pusta!\n";
            }
            else if (car.Production_year >= DateTime.Now)
            {
                valid = false;
                errorMsg += "Data produkcji nie może być większa niż obecna!\n";
            }

            if (String.IsNullOrWhiteSpace(car.Engine_type))
            {
                valid = false;
                errorMsg += "Silnik nie może zostać pusty!\n";
            }

            if (car.Rent_cost <= 0)
            {
                valid = false;
                errorMsg += "Koszt wynajęcia samochodu nie może być mniejszy lub równy 0!\n";
            }

            validationResult.Add(valid, errorMsg);
            return validationResult;
        }

        /// <summary>
        /// Validates if car data are correct.
        /// </summary>
        /// <returns>Dictionary with key = ture if data are correct, key = false if data are incorrect.</returns>
        //public Dictionary<bool, string> SimpleValidateCar()
        //{
        //    bool valid = true;
        //    string errorMsg = string.Empty;
        //    Dictionary<bool, string> validationResult = new Dictionary<bool, string>();

        //    if (String.IsNullOrWhiteSpace(Category))
        //    {
        //        valid = false;
        //        errorMsg += "Klasa samochodu nie może zostać pusta!\n";
        //    }

        //    if (String.IsNullOrWhiteSpace(Manufacturer))
        //    {
        //        valid = false;
        //        errorMsg += "Marka samochodu nie może zostać pusta!\n";
        //    }

        //    if (String.IsNullOrWhiteSpace(Model))
        //    {
        //        valid = false;
        //        errorMsg += "Model samochodu nie może zostać pusty!\n";
        //    }

        //    if (String.IsNullOrWhiteSpace(DriveType))
        //    {
        //        valid = false;
        //        errorMsg += "Rodzaj napędu nie może zostać pusty!\n";
        //    }

        //    if (String.IsNullOrWhiteSpace(Gearbox))
        //    {
        //        valid = false;
        //        errorMsg += "Skrzynia biegów nie może zostać pusta!\n";
        //    }

        //    if (ProductionDate == null)
        //    {
        //        valid = false;
        //        errorMsg += "Data produkcji nie może zostać pusta!\n";
        //    }
        //    else if (ProductionDate >= DateTime.Now)
        //    {
        //        valid = false;
        //        errorMsg += "Data produkcji nie może być większa niż obecna!\n";
        //    }

        //    if (String.IsNullOrWhiteSpace(Engine))
        //    {
        //        valid = false;
        //        errorMsg += "Silnik nie może zostać pusty!\n";
        //    }

        //    decimal cost;
        //    if (String.IsNullOrEmpty(Cost))
        //    {
        //        valid = false;
        //        errorMsg += "Koszt wynajęcia samochodu nie może zostać pusty!\n";
        //    }
        //    else if (!decimal.TryParse(Cost, out cost))
        //    {
        //        valid = false;
        //        errorMsg += "Podany koszt wynajęcia samochodu jest nieprawidłowy!\n";
        //    }
        //    else if (cost <= 0)
        //    {
        //        valid = false;
        //        errorMsg += "Koszt wynajęcia samochodu nie może być mniejszy lub równy 0!\n";
        //    }

        //    if (ValidateVINNumber().Keys.Contains(false))
        //    {
        //        valid = false;
        //        errorMsg += ValidateVINNumber()[false];
        //    }

        //    if (ValidateLicensePlatenumber().Keys.Contains(false))
        //    {
        //        valid = false;
        //        errorMsg += ValidateLicensePlatenumber()[false];
        //    }

        //    validationResult.Add(valid, errorMsg);
        //    return validationResult;
        //}

        /// <summary>
        /// Validates if VIN number is correct.
        /// </summary>
        /// <returns>Dictionary with key = ture if VIN number is correct, key = false if VIN number is incorrect.</returns>
        public static Dictionary<Boolean, string> ValidateVINNumber(this Car car)
        {
            Dictionary<Boolean, string> validationResult = new Dictionary<bool, string>();
            Boolean isValid = true;
            string errorMsg = "";

            if (String.IsNullOrWhiteSpace(car.Vin))
            {
                isValid = false;
                errorMsg += "Numer VIN samochodu nie może zostać pusty!\n";
            }
            else if (car.Vin.Trim().Length != 17)
            {
                isValid = false;
                errorMsg += $"Wprowadzony numer VIN samochodu jest nieprawiłowy! ({car.Vin.Trim().Length} znaków zamiast 17)\n";
            }
            else if (car.Vin.Contains("I") || car.Vin.Contains("O") || car.Vin.Contains("Q"))
            {
                isValid = false;
                errorMsg += $"Wprowadzony numer VIN samochodu jest nieprawiłowy! (Numer VIN nie może zawierać liter I, O i Q!\n";
            }

            validationResult.Add(isValid, errorMsg);
            return validationResult;
        }

        /// <summary>
        /// Validates if license plate number is correct.
        /// </summary>
        /// <returns>Dictionary with key = ture if license plate number is correct, key = false if license plate number is incorrect.</returns>
        public static Dictionary<Boolean, string> ValidateLicensePlatenumber(this Car car)
        {
            Dictionary<Boolean, string> validationResult = new Dictionary<bool, string>();
            Boolean valid = true;
            string errorMsg = string.Empty;

            if (String.IsNullOrWhiteSpace(car.License_plate_number))
            {
                valid = false;
                errorMsg += "Numer rejestracyjny samochodu nie może zostać pusty!";
            }
            else if (car.License_plate_number.Trim().Length != 7)
            {
                valid = false;
                errorMsg += $"Wprowadzony numer rejestracyjny samochodu jest nieprawiłowy! ({car.License_plate_number.Trim().Length} znaków zamiast 7)";
            }
            else if (!Char.IsLetter(car.License_plate_number.ToCharArray()[0]) || !Char.IsLetter(car.License_plate_number.ToCharArray()[1]))
            {
                valid = false;
                errorMsg += $"Wprowadzony numer rejestracyjny samochodu jest nieprawiłowy! (Co najmniej 2 pierwsze znaki muszą być literami)";
            }

            validationResult.Add(valid, errorMsg);
            return validationResult;
        }

        /// <summary>
        /// Prepare data for comboBox controls used in some forms (like MainWindowForm and AddNewCarForm).
        /// </summary>
        /// <returns>Dictionary with all possible values for each comboBox control.</returns>
        //public static Dictionary<string, string[]> FillComboBoxes()
        //{
        //    Dictionary<string, string[]> comboBoxes = new Dictionary<string, string[]>();
        //    List<string> categoryComboBoxValues = new List<string>();
        //    List<string> driveTypeComboBoxValues = new List<string>();
        //    List<string> engineTypeComboBoxValues = new List<string>();
        //    List<string> gearboxComboBoxValues = new List<string>();

        //    foreach (EnumCategory e in Enum.GetValues(typeof(EnumCategory)).Cast<EnumCategory>())
        //    {
        //        categoryComboBoxValues.Add(e.ToString());
        //    }
        //    foreach (EnumDriveType e in Enum.GetValues(typeof(EnumDriveType)).Cast<EnumDriveType>())
        //    {
        //        driveTypeComboBoxValues.Add(e.ToString());
        //    }
        //    foreach (EnumEngineType e in Enum.GetValues(typeof(EnumEngineType)).Cast<EnumEngineType>())
        //    {
        //        engineTypeComboBoxValues.Add(e.ToString());
        //    }
        //    foreach (EnumGearboxType e in Enum.GetValues(typeof(EnumGearboxType)).Cast<EnumGearboxType>())
        //    {
        //        gearboxComboBoxValues.Add(e.ToString());
        //    }

        //    comboBoxes.Add("category", categoryComboBoxValues.ToArray());
        //    comboBoxes.Add("driveType", driveTypeComboBoxValues.ToArray());
        //    comboBoxes.Add("engine", engineTypeComboBoxValues.ToArray());
        //    comboBoxes.Add("gearbox", gearboxComboBoxValues.ToArray());

        //    return comboBoxes;
        //}

    }
}
