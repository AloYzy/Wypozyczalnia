namespace Wypozyczalnia.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Car
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Car()
        {
            Rents = new HashSet<Rent>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Type { get; set; }

        [Required]
        [StringLength(20)]
        public string Manufacturer { get; set; }

        [Required]
        [StringLength(20)]
        public string Model { get; set; }

        [Column(TypeName = "date")]
        public DateTime Production_year { get; set; }

        [Required]
        [StringLength(20)]
        public string Drive_type { get; set; }

        [Required]
        [StringLength(20)]
        public string Gearbox_type { get; set; }

        [Required]
        [StringLength(20)]
        public string Engine_type { get; set; }

        [Required]
        [StringLength(20)]
        public string Status { get; set; }

        public decimal Rent_cost { get; set; }

        [Required]
        [StringLength(17)]
        public string Vin { get; set; }

        [Required]
        [StringLength(7)]
        public string License_plate_number { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rent> Rents { get; set; }
    }
}
