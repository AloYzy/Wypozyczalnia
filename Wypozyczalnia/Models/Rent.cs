namespace Wypozyczalnia.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Rent
    {
        public int Id { get; set; }

        public int Car_id { get; set; }

        public int Client_id { get; set; }

        [Column(TypeName = "date")]
        public DateTime Start_date { get; set; }

        [Column(TypeName = "date")]
        public DateTime End_date { get; set; }

        public decimal Total_cost { get; set; }

        public virtual Car Car { get; set; }

        public virtual Client Client { get; set; }
    }
}
