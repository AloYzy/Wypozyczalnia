﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wypozyczalnia.Models;

namespace Wypozyczalnia.Views
{
    public interface IShowCars
    {
        CheckedListBox.CheckedItemCollection CategoryItems { get; }
        CheckedListBox CategoryCheckedListBox { get; set; }
        string CostTextFrom { get; }
        string CostTextTo { get; }
        CheckedListBox.CheckedItemCollection DriveTypeItems { get; }
        CheckedListBox DriveTypeCheckedListBox { get; set; }
        CheckedListBox.CheckedItemCollection EngineTypeItems { get; }
        CheckedListBox EngineCheckedListBox { get; set; }
        string GearboxSelectionText { get; }
        CheckedListBox.CheckedItemCollection ManufacturerItems { get; }
        CheckedListBox ManufacturerCheckedListBox { get; set; }
        CheckedListBox.CheckedItemCollection ModelItems { get; }
        CheckedListBox ModelCheckedListBox { get; set; }
        DateTime ProductionDateFrom { get; }
        DateTime ProductionDateTo { get; }
        ICollection<Car> AllAvailableCars { set; }
        ICollection<Car> SearchedAvailableCars { set; }
        ICollection<Car> AllCars { set; }
    }
}
