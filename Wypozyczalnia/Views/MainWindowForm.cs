﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wypozyczalnia.Models;
using Wypozyczalnia.Presenters;
using Wypozyczalnia.Views;

namespace Wypozyczalnia
{
    public partial class MainWindowForm : Form, IShowCars
    {
        public CheckedListBox.CheckedItemCollection CategoryItems => categoryCheckedListBox.CheckedItems;

        public string CostTextFrom => costFromTextBox.Text;

        public string CostTextTo => costToTextBox.Text;

        public CheckedListBox.CheckedItemCollection DriveTypeItems => driveTypeCheckedListBox.CheckedItems;

        public CheckedListBox.CheckedItemCollection EngineTypeItems => engineCheckedListBox.CheckedItems;

        public string GearboxSelectionText => gearboxRadioButton1.Checked ? "('" + gearboxRadioButton1.Text + "')" : (gearboxRadioButton2.Checked ? "('" + gearboxRadioButton2.Text + "')" : ($"('{gearboxRadioButton1.Text}', '{gearboxRadioButton2.Text}')"));

        public CheckedListBox.CheckedItemCollection ManufacturerItems => manufacturerCheckedListBox.CheckedItems;

        public CheckedListBox.CheckedItemCollection ModelItems => modelCheckedListBox.CheckedItems;

        public DateTime ProductionDateFrom => productionDateFrom.Value;

        public DateTime ProductionDateTo => productionDateTo.Value;

        public ICollection<Car> AllAvailableCars { set => availableCarsDataGridView.DataSource = value; }

        public ICollection<Car> SearchedAvailableCars { set => searchedAvailableCarsDataGridView.DataSource = value; }

        public ICollection<Car> AllCars { set => allCarsDataGridView.DataSource = value; }

        public CheckedListBox CategoryCheckedListBox
        {
            get
            {
                return this.categoryCheckedListBox;
            }
            set { }
        }

        public CheckedListBox DriveTypeCheckedListBox
        {
            get
            {
                return this.driveTypeCheckedListBox;
            }
            set { }
        }

        public CheckedListBox EngineCheckedListBox
        {
            get
            {
                return this.engineCheckedListBox;
            }
            set { }
        }

        public CheckedListBox ManufacturerCheckedListBox
        {
            get
            {
                return this.manufacturerCheckedListBox;
            }
            set { }
        }

        public CheckedListBox ModelCheckedListBox
        {
            get
            {
                return this.modelCheckedListBox;
            }
            set { }
        }

        public MainWindowForm()
        {
            InitializeComponent();
        }

        private void ShowAllCarsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.showCarsPanel.Visible = true;
            this.searchCarsPanel.Visible = false;

            SearchCarPresenter presenter = new SearchCarPresenter(this);
            presenter.ShowAllCars();
        }

        private void RentCarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.searchCarsPanel.Visible = true;
            this.showCarsPanel.Visible = false;

            SearchCarPresenter presenter = new SearchCarPresenter(this);
            presenter.SearchAllAvailableCars();
            presenter.FillCheckedListBoxes();
        }

        private void MainWindowForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'wypozyczalniaDataSet.Samochody' table. You can move, or remove it, as needed.
            //this.samochodyTableAdapter.Fill(this.wypozyczalniaDataSet.Samochody);

        }

        private void AvailableCarsDataGridView_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.ShowRentCarForm(sender, e);
        }

        private void DodajSamochodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form addNewCarForm = new AddNewCarForm();
            addNewCarForm.ShowDialog();
        }

        private void EdytujSamochodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form modifyCarForm = new ModifyCarForm();
            modifyCarForm.ShowDialog();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            SearchCarPresenter searchCarPresenter = new SearchCarPresenter(this);
            searchCarPresenter.SearchAvailableCarsByCriteria(CategoryItems, ManufacturerItems, ModelItems, DriveTypeItems, EngineTypeItems, ProductionDateFrom, ProductionDateTo, CostTextFrom, CostTextTo, GearboxSelectionText);
        }

        private void searchedAvailableCarsDataGridView_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.ShowRentCarForm(sender, e);
        }

        private void ShowRentCarForm(object sender, DataGridViewCellEventArgs e)
        {
            Form rentCarForm = new RentForm(sender, e);
            rentCarForm.ShowDialog();

        }

        private void OdbierzSamochodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form returnCarForm = new ReturnCarForm();
            returnCarForm.ShowDialog();
        }

        private void usunSamochodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form deleteCarForm = new DeleteCarForm();
            deleteCarForm.ShowDialog();
        }
    }
}
