﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wypozyczalnia.Views
{
    public interface ILogin
    {
        string LoginText { get; }
        string PasswordText { get; }
        bool LogedIn { get; set; }
    }
}
