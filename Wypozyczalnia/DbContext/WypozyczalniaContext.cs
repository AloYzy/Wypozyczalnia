namespace Wypozyczalnia
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Wypozyczalnia.Models;

    public partial class WypozyczalniaContext : DbContext
    {
        private static WypozyczalniaContext instance = null;
        public static WypozyczalniaContext Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new WypozyczalniaContext();
                }
                return instance;
            }
        }

        private WypozyczalniaContext()
            : base("name=WypozyczalniaEFModel")
        {
        }

        public virtual DbSet<Car> Cars { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Rent> Rents { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Car>()
                .Property(e => e.Type)
                .IsFixedLength();

            modelBuilder.Entity<Car>()
                .Property(e => e.Manufacturer)
                .IsFixedLength();

            modelBuilder.Entity<Car>()
                .Property(e => e.Model)
                .IsFixedLength();

            modelBuilder.Entity<Car>()
                .Property(e => e.Drive_type)
                .IsFixedLength();

            modelBuilder.Entity<Car>()
                .Property(e => e.Gearbox_type)
                .IsFixedLength();

            modelBuilder.Entity<Car>()
                .Property(e => e.Engine_type)
                .IsFixedLength();

            modelBuilder.Entity<Car>()
                .Property(e => e.Status)
                .IsFixedLength();

            modelBuilder.Entity<Car>()
                .Property(e => e.Vin)
                .IsFixedLength();

            modelBuilder.Entity<Car>()
                .Property(e => e.License_plate_number)
                .IsFixedLength();

            modelBuilder.Entity<Car>()
                .HasMany(e => e.Rents)
                .WithRequired(e => e.Car)
                .HasForeignKey(e => e.Car_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Client>()
                .Property(e => e.Surname)
                .IsFixedLength();

            modelBuilder.Entity<Client>()
                .Property(e => e.License_number)
                .IsFixedLength();

            modelBuilder.Entity<Client>()
                .HasMany(e => e.Rents)
                .WithRequired(e => e.Client)
                .HasForeignKey(e => e.Client_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Login)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Password)
                .IsUnicode(false);
        }
    }
}
