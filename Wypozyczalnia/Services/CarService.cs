﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wypozyczalnia.Enums;
using Wypozyczalnia.Extensions;
using Wypozyczalnia.Models;
using Wypozyczalnia.Presenters;

namespace Wypozyczalnia.Services
{
    public class CarService
    {
        private readonly WypozyczalniaContext wypozyczalniaContext;
        public CarService()
        {
            wypozyczalniaContext = WypozyczalniaContext.Instance;
        }

        /// <summary>
        /// Save new car to database.
        /// </summary>
        public void AddNewCar(Car car)
        {
            wypozyczalniaContext.Cars.Add(car);
            wypozyczalniaContext.SaveChanges();
        }

        /// <summary>
        /// Reads database and searches for all Car.
        /// </summary>
        /// <returns>List that contains all Car.</returns>
        public ICollection<Car> GetAllCars()
        {
            var allCars = wypozyczalniaContext.Cars.ToList();
            return allCars;
        }

        /// <summary>
        /// Reads database and searches for all available Car.
        /// </summary>
        /// <returns>List that contains all available Car.</returns>
        public ICollection<Car> GetAllAvailableCars()
        {
            var allAvailableCars = wypozyczalniaContext.Cars.Where(c => c.Status.Equals(CarEnums.EnumStatus.Dostępny.ToString())).ToList();
            return allAvailableCars;
        }

        /// <summary>
        /// Reads database and searches for available Car based on given criteria.
        /// </summary>
        /// <param name="searchCriteria">Criteria searched from search screen</param>
        /// <returns>List of found cars that meet specified criteria.</returns>
        //public ICollection<Car> SearchAvailableCarsByCriteria(SearchCarPresenter.SearchCriteria searchCriteria)
        //{
        //    StringBuilder sqlQuery = new StringBuilder("SELECT * FROM Car WHERE ");
        //    if (!string.IsNullOrEmpty(searchCriteria.CategoryCriteria))
        //    {
        //        sqlQuery.Append($"type IN ({searchCriteria.CategoryCriteria}) AND ");
        //    }
        //    if (!string.IsNullOrEmpty(searchCriteria.ManufacturerCriteria))
        //    {
        //        sqlQuery.Append($"manufacturer IN ({searchCriteria.ManufacturerCriteria}) AND ");
        //    }
        //    if (!string.IsNullOrEmpty(searchCriteria.ModelCriteria))
        //    {
        //        sqlQuery.Append($"model IN ({searchCriteria.ModelCriteria}) AND ");
        //    }
        //    if (!string.IsNullOrEmpty(searchCriteria.DriveTypeCriteria))
        //    {
        //        sqlQuery.Append($"drive_type IN ({searchCriteria.DriveTypeCriteria}) AND ");
        //    }
        //    if (!string.IsNullOrEmpty(searchCriteria.EngineTypeCriteria))
        //    {
        //        sqlQuery.Append($"engine_type IN ({searchCriteria.EngineTypeCriteria}) AND ");
        //    }
        //    if (!string.IsNullOrEmpty(searchCriteria.GearboxCriteria))
        //    {
        //        sqlQuery.Append($"gearbox_type IN {searchCriteria.GearboxCriteria} AND ");
        //    }
        //    sqlQuery.Append($"(production_year >= '{searchCriteria.ProductionDateFromCriteria}' AND production_year <= '{searchCriteria.ProductionDateToCriteria}') AND ");
        //    sqlQuery.Append($"(rent_cost BETWEEN {searchCriteria.CostFromCriteria.ToString().Replace(',', '.')} AND {searchCriteria.CostToCriteria.ToString().Replace(',', '.')}) AND ");
        //    sqlQuery.Append($"status = '{EnumStatus.Dostępny.ToString()}' ");
        //    sqlQuery.Append(" ORDER BY id ASC");

        //    DataTable dataTable = new DataTable();

        //    using (SqlConnection sqlConnection = new SqlConnection(connectionString))
        //    {
        //        sqlConnection.Open();

        //        var sqlDataAdaper = new SqlDataAdapter(sqlQuery.ToString(), sqlConnection);
        //        sqlDataAdaper.Fill(dataTable);
        //    }

        //    return dataTable;
        //}

        public Dictionary<bool, string> ValidateCar(Car car)
        {
            bool isValid = car.ValidateCar().ContainsKey(true);
            string errorMsg = car.ValidateCar()[isValid];

            if (car.ValidateVINNumber().Keys.Contains(false))
            {
                isValid = false;
                errorMsg += car.ValidateVINNumber()[false];
            }
            else if (IsVINPresentInDB(car.Vin))
            {
                isValid = false;
                errorMsg += $"Wprowadzony numer VIN już istnieje w bazie!";
            }

            if (car.ValidateLicensePlatenumber().Keys.Contains(false))
            {
                isValid = false;
                errorMsg += car.ValidateLicensePlatenumber()[false];
            }
            else if (IsLicensePlateNumberPresentInDB(car.License_plate_number))
            {
                isValid = false;
                errorMsg += $"Wprowadzony numer rejestracyjny samochodu już istnieje w bazie!";
            }

            return new Dictionary<bool, string> { { isValid, errorMsg } };
        }

        public void UpdateCar(Car car)
        {
            var foundCar = wypozyczalniaContext.Cars.Where(c => c.Id == car.Id).FirstOrDefault();

            foundCar.Type = car.Type;
            foundCar.Manufacturer = car.Manufacturer;
            foundCar.Model = car.Model;
            foundCar.Production_year = car.Production_year;
            foundCar.Drive_type = car.Drive_type;
            foundCar.Engine_type = car.Engine_type;
            foundCar.Gearbox_type = car.Gearbox_type;
            foundCar.License_plate_number = car.License_plate_number;
            foundCar.Vin = car.Vin;
            foundCar.Rent_cost = car.Rent_cost;

            wypozyczalniaContext.SaveChanges();
        }

        /// <summary>
        /// Reads database and searches car based on given license plate number.
        /// </summary>
        /// <param name="licensePlateNum">String that represents license plate number.</param>
        /// <returns>Car object.</returns>
        public Car GetCarByLicensePlateNum(string licensePlateNum)
        {
            var car = wypozyczalniaContext.Cars.Where(c => c.License_plate_number.Equals(licensePlateNum)).FirstOrDefault();
            return car;
        }

        /// <summary>
        /// Reads database and searches car based on given VIN number.
        /// </summary>
        /// <param name="VIN">String that represents VIN number.</param>
        /// <returns>Car object.</returns>
        public Car GetCarByVIN(string VIN)
        {
            var car = wypozyczalniaContext.Cars.Where(c => c.Vin.Equals(VIN)).FirstOrDefault();
            return car;
        }

        /// <summary>
        /// Reads database and searches for all categories already present in database.
        /// </summary>
        /// <returns>List that contains all categories present in database.</returns>
        public ICollection<string> GetAllCategories()
        {
            var categories = wypozyczalniaContext.Cars.Select(c => c.Type).Distinct().OrderBy(type => type).ToList();
            return categories;
        }

        /// <summary>
        /// Reads database and searches for all drive types already present in database.
        /// </summary>
        /// <returns>List that contains all drive types present in database.</returns>
        public ICollection<string> GetAllDriveTypes()
        {
            var driveTypes = wypozyczalniaContext.Cars.Select(c => c.Drive_type).Distinct().OrderBy(type => type).ToList();
            return driveTypes;
        }

        /// <summary>
        /// Reads database and searches for all engine types already present in database.
        /// </summary>
        /// <returns>List that contains all engine types present in database.</returns>
        public ICollection<string> GetAllEngineTypes()
        {
            var engineTypes = wypozyczalniaContext.Cars.Select(c => c.Engine_type).Distinct().OrderBy(type => type).ToList();
            return engineTypes;
        }

        /// <summary>
        /// Reads database and searches for all gearbox types already present in database.
        /// </summary>
        /// <returns>List that contains all gearbox types present in database.</returns>
        public ICollection<string> GetAllGearboxTypes()
        {
            var gearboxTypes = wypozyczalniaContext.Cars.Select(c => c.Gearbox_type).Distinct().OrderBy(type => type).ToList();
            return gearboxTypes;
        }

        /// <summary>
        /// Reads database and searches for all manufacturers already present in database.
        /// </summary>
        /// <returns>List that contains all manufacturers present in database.</returns>
        public ICollection<string> GetAllManufacturers()
        {
            var manufacturers = wypozyczalniaContext.Cars.Select(c => c.Manufacturer).Distinct().OrderBy(manufacturer => manufacturer).ToList();
            return manufacturers;
        }

        /// <summary>
        /// Reads database and searches for all models already present in database.
        /// </summary>
        /// <returns>List that contains all models present in database.</returns>
        public ICollection<string> GetAllModels()
        {
            var models = wypozyczalniaContext.Cars.Select(c => c.Model).Distinct().OrderBy(model => model).ToList();
            return models;
        }

        /// <summary>
        /// Reads database and searches the most expensive car.
        /// </summary>
        /// <returns>Decimal that represents the most expensive car.</returns>
        public decimal GetMaxCost()
        {
            var maxCost = wypozyczalniaContext.Cars.Select(c => c.Rent_cost).Max();
            return maxCost;
        }

        /// <summary>
        /// Checks if license plate number is already present in database.
        /// </summary>
        /// <returns>True if license plate number is already present in database, false if license plate number is not present in database.</returns>
        public bool IsLicensePlateNumberPresentInDB(string licensePlate)
        {
            var exists = wypozyczalniaContext.Cars.Select(c => c.License_plate_number).Contains(licensePlate);
            return exists;
        }

        /// <summary>
        /// Checks if VIN number is already present in database.
        /// </summary>
        /// <returns>True if VIN number is already present in database, false if VIN number is not present in database.</returns>
        public bool IsVINPresentInDB(string VIN)
        {
            var exists = wypozyczalniaContext.Cars.Select(c => c.Vin).Contains(VIN);
            return exists;
        }

        /// <summary>
        /// Updates car data in database.
        /// </summary>
        /// <param name="id">Integer that represents id of car.</param>
        /// <returns>True if update succeeded, false if update failed.</returns>
        //public bool UpdateCar(int id)
        //{
        //    bool success = false;

        //    using (SqlConnection sqlConnection = new SqlConnection())
        //    {
        //        sqlConnection.ConnectionString = Helpers.connectionString;
        //        sqlConnection.Open();

        //        SqlCommand command = new SqlCommand($"UPDATE samochody SET Klasa = @category, " +
        //                                                                $"Marka = @manufacturer, " +
        //                                                                $"Model = @model, " +
        //                                                                $"[Rok produkcji] = @date, " +
        //                                                                $"[Rodzaj napędu] = @driveType, " +
        //                                                                $"[Skrzynia biegów] = @gearbox, " +
        //                                                                $"Silnik = @engine, " +
        //                                                                $"[Koszt wynajęcia] = @cost, " +
        //                                                                $"VIN = @vin, " +
        //                                                                $"[Numer rejestracyjny] = @licensePlateNum " +
        //                                                                $"WHERE Id = @id", sqlConnection);

        //        command.Parameters.Add(new SqlParameter("category", Category));
        //        command.Parameters.Add(new SqlParameter("manufacturer", Manufacturer));
        //        command.Parameters.Add(new SqlParameter("model", Model));
        //        command.Parameters.Add(new SqlParameter("date", ProductionDate.ToString("yyyy-MM-dd")));
        //        command.Parameters.Add(new SqlParameter("driveType", DriveType));
        //        command.Parameters.Add(new SqlParameter("gearbox", Gearbox));
        //        command.Parameters.Add(new SqlParameter("engine", Engine));
        //        command.Parameters.Add(new SqlParameter("cost", SqlDbType.Decimal)
        //        {
        //            Precision = 18,
        //            Scale = 2
        //        }).Value = Cost;
        //        command.Parameters.Add(new SqlParameter("vin", VIN));
        //        command.Parameters.Add(new SqlParameter("licensePlateNum", LicensePlateNum));
        //        command.Parameters.Add(new SqlParameter("id", id));

        //        try
        //        {
        //            command.ExecuteNonQuery();
        //            success = true;
        //        }
        //        catch (Exception exp)
        //        {

        //            throw;
        //        }
        //    }

        //    return success;
        //}

        /// <summary>
        /// Change car status in database as "rent".
        /// </summary>
        /// <param name="car">Car object.</param>
        public void SetStatusAsRent(Car car)
        {
            var foundCar = wypozyczalniaContext.Cars.Find(car.Id);
            foundCar.Status = CarEnums.EnumStatus.Wypożyczony.ToString();
            wypozyczalniaContext.SaveChanges();
        }

        /// <summary>
        /// Change car status in database as "available".
        /// </summary>
        /// <param name="car">Car object.</param>
        public void SetStatusAsAvailable(Car car)
        {
            var foundCar = wypozyczalniaContext.Cars.Find(car.Id);
            foundCar.Status = CarEnums.EnumStatus.Dostępny.ToString();
            wypozyczalniaContext.SaveChanges();
        }

        /// <summary>
        /// Change car status in database as "deleted".
        /// </summary>
        /// <param name="car">Car object.</param>
        public void SetStatusAsDeleted(Car car)
        {
            var foundCar = wypozyczalniaContext.Cars.Find(car.Id);
            foundCar.Status = CarEnums.EnumStatus.Usunięty.ToString();
            wypozyczalniaContext.SaveChanges();
        }

    }
}
