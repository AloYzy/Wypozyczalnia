﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wypozyczalnia.Models;

namespace Wypozyczalnia.Services
{
    public class ClientService
    {
        private readonly WypozyczalniaContext wypozyczalniaContext;

        public ClientService()
        {
            wypozyczalniaContext = WypozyczalniaContext.Instance;
        }

        /// <summary>
        /// Saves new client to database.
        /// </summary>
        /// <param name="clientName">String that represent client's name.</param>
        /// <param name="clientSurname">String that represent client's surname.</param>
        /// <param name="licenseNumber">String that represent client's license number.</param>
        /// <returns>Client object that represents newly added client.</returns>
        public Client AddNewClient(string clientName, string clientSurname, string licenseNumber)
        {
            Client client = new Client { Name = clientName, Surname = clientSurname, License_number = licenseNumber };
            wypozyczalniaContext.Clients.Add(client);
            wypozyczalniaContext.SaveChanges();

            return client;
        }

        /// <summary>
        /// Reads database and seraches for client based on given license number.
        /// </summary>
        /// <param name="licenseNumber">String that represent client's license number.</param>
        /// <returns>Client object.</returns>
        public Client GetClientByLicenceNumber(string licenseNumber)
        {
            var client = wypozyczalniaContext.Clients.Where(c => c.License_number.Equals(licenseNumber)).FirstOrDefault();
            return client;
        }

        /// <summary>
        /// Reads database and seraches for client based on given id.
        /// </summary>
        /// <param name="id">Integer that represent client record in database.</param>
        /// <returns>Client object.</returns>
        public Client GetClientById(int id)
        {
            var client = wypozyczalniaContext.Clients.Where(c => c.Id == id).FirstOrDefault();
            return client;
        }

    }
}
