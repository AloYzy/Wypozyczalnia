﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wypozyczalnia.Models;

namespace Wypozyczalnia.Services
{
    public class LoginService
    {
        private readonly WypozyczalniaContext wypozyczalniaContext;
        public LoginService()
        {
            wypozyczalniaContext = WypozyczalniaContext.Instance;
        }

        public bool IsUserLoggedIn(User user)
        {
            bool isLoggedIn = false;

            var foundUser = wypozyczalniaContext.Users
                                                .Where(u => u.Login.Equals(user.Login) && u.Password.Equals(user.Password))
                                                .FirstOrDefault();

            if(foundUser != null)
            {
                isLoggedIn = true;
            }

            return isLoggedIn;
        }
    }
}
