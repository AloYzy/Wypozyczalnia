﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wypozyczalnia.Models;

namespace Wypozyczalnia.Services
{
    public class RentService
    {
        private readonly WypozyczalniaContext wypozyczalniaContext;

        public RentService()
        {
            wypozyczalniaContext = WypozyczalniaContext.Instance;
        }
        /// <summary>
        /// Saves new car rent to database.
        /// </summary>
        public void CreateNewRent(Rent rent)
        {
            wypozyczalniaContext.Rents.Add(rent);
            wypozyczalniaContext.SaveChanges();
        }

        /// <summary>
        /// Reads database and serached for rent based on given car id.
        /// </summary>
        /// <param name="id">Integer that represent car id.</param>
        /// <returns>Rent object.</returns>
        public Rent GetRentByCarId(int id)
        {
            var rent = wypozyczalniaContext.Rents.Where(r => r.Car_id == id).FirstOrDefault();
            return rent;
        }
    }
}
